import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from '../users.service';


@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {
  userLoginRef=new FormGroup({
    email:new FormControl(),
    passward:new FormControl()
  });
  constructor(public users:UsersService,public router:Router) { }
  user:string="";
  ngOnInit(): void {
  }
  checkUser(){
    let login=this.userLoginRef.value
    console.log(login.email,login.passward);
    this.users.userLoginDetails(login.email,login.passward).subscribe(res=>this.user=res);
      this.router.navigate(["book-display"])
    
  }
}
