import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Admin } from './admin';

@Injectable({
  providedIn: 'root'
})
export class AdminsService {

  constructor(public http:HttpClient) { }
  storeAdminDetails(admin:AdminsService):Observable<string>{
    return this.http.post("http://localhost:8181/admin/registeradmin",admin,{responseType:'text'})
  }
  adminLoginDetails(username:any,passward:string):Observable<string>{
    return this.http.get("http://localhost:8181/admin/loginadmin/"+username/+passward,{responseType:'text'})
 
  }
}
