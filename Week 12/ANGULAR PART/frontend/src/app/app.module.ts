import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserloginComponent } from './userlogin/userlogin.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AdminsignupComponent } from './adminsignup/adminsignup.component';
import { UsersignupComponent } from './usersignup/usersignup.component';
import { BooksComponent } from './books/books.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserDisplayComponent } from './user-display/user-display.component';
import { BookDisplayComponent } from './book-display/book-display.component';
@NgModule({
  declarations: [
    AppComponent,
    UserloginComponent,
    AdminloginComponent,
    AdminsignupComponent,
    UsersignupComponent,
    BooksComponent,
    UserDisplayComponent,
    BookDisplayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,FormsModule,ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
