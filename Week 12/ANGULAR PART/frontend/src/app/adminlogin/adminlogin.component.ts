import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminsService } from '../admins.service';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {
  adminLoginRef=new FormGroup({
    username:new FormControl(),
    passward:new FormControl()
  });
  storeMsg:string="";
  constructor(public admin:AdminsService,public router:Router) { }

  ngOnInit(): void {
  }
  checkAdmin(){
    let login=this.adminLoginRef.value
    console.log(login.username,login.passward);
   this.admin.adminLoginDetails(login.username,login.passward).subscribe(res=>this.storeMsg=res)
   this.router.navigate(["user-display"])
    
  }
}
