export class Allbook {
    constructor(
        public bookid:number,
        public bname:string,
        public author:string,
        public geners:string
    ) {}
}
