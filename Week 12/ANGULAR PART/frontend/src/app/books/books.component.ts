import { Component, OnInit } from '@angular/core';
import { Allbook } from '../allbook';
//import {Books} from '../books';
import { BookserviceService } from '../bookservice.service';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  bookss:Array<Allbook>=[];
  constructor(public bookser:BookserviceService) { }

  ngOnInit(): void {
    this.loadFoods();
  }
  loadFoods():void{
   // console.log(":event fired")
    this.bookser.loadBookDetails().subscribe(res=>this.bookss=res );
  }
}
