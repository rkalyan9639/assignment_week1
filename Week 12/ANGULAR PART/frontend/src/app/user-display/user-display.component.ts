import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { Userservice } from '../userservice';

@Component({
  selector: 'app-user-display',
  templateUrl: './user-display.component.html',
  styleUrls: ['./user-display.component.css']
})
export class UserDisplayComponent implements OnInit {
  user:Array<Userservice>=[];
  deleteMsg:string=""
  updateMsg:string=""
  flag:boolean=false
  uid:number=0;
  name:string=""
  email:string=""

  
  constructor(public userdisp:UsersService) { }

  ngOnInit(): void {
    this.loadUsers();
  }
  loadUsers():void{
    // console.log(":event fired")
     this.userdisp.loadUserDisplay().subscribe(res=>this.user=res)
   }
   deleteUser(uid:number){
     console.log(uid)
     this.userdisp.deleteUsersInfo(uid).subscribe(res=>this.deleteMsg=res,()=>this.loadUsers())
   }
   updateUser(uu:Userservice){
    console.log(uu)
    this.flag=true
    this.uid=uu.uid
    this.name=uu.name
    this.email=uu.email

    //this.userdisp.deleteUsersInfo(uid).subscribe(res=>this.deleteMsg=res,()=>this.loadUsers())
  }
  updateUserName(){
    let uu={"uid":this.uid,"name":this.name,"email":this.email}
    console.log(uu)
    this.userdisp.updateUsersInfo(uu).subscribe(res=>this.updateMsg=res,
      ()=>{
      this.loadUsers();
    this.flag=false
  })
  }
}
