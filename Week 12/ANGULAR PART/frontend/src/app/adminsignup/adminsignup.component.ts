import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdminsService } from '../admins.service';

@Component({
  selector: 'app-adminsignup',
  templateUrl: './adminsignup.component.html',
  styleUrls: ['./adminsignup.component.css']
})
export class AdminsignupComponent implements OnInit {
  storeMsg:string="";
  constructor(public admin:AdminsService) { }

  ngOnInit(): void {
  }
  adminStore(adminRef:NgForm){
     console.log(adminRef.value);
     this.admin.storeAdminDetails(adminRef.value).
     subscribe(res=>this.storeMsg=res)
   }
}
