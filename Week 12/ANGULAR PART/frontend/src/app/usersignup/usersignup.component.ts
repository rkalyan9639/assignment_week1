import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsersService } from '../users.service';
import { Userservice } from '../userservice';

@Component({
  selector: 'app-usersignup',
  templateUrl: './usersignup.component.html',
  styleUrls: ['./usersignup.component.css']
})
export class UsersignupComponent implements OnInit {
  storeMsg:string="";
  constructor(public user:UsersService) { }

  ngOnInit(): void {
  }
  storeUser(userRef:NgForm){
   // console.log(userRef.value);
    this.user.storeUserDetails(userRef.value).
    subscribe(res=>this.storeMsg=res)
  }
}
