package com.grtlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.grtlearning.bean.Credential;
@Repository
public interface CdredentialsDao extends JpaRepository<Credential, String> {
	@Query("select aa from Credential aa where aa.email=:email and aa.passward= :passward")
	Credential existsByEmail(@Param("email") String email,@Param("passward") String passward) ;
	
	@Query("delete  from Credential aa where aa.email=:email")
	int deleteByEmail(@Param("email") String email);



}
