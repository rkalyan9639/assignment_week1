package com.grtlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.grtlearning.bean.Foods;
import com.grtlearning.dao.FoodDao;

@Service
public class FoodService {
	@Autowired
	FoodDao foodDao;
	public List<Foods> getListOfFood(){
		return foodDao.findAll();
	}
	public int adminDelInfo(int id) {
		if(!foodDao.existsById(id)) {
			return 0;
		}else {
			foodDao.deleteById(id);
			return 1;
		}
	}
	public int storeFood(Foods cre) {
		if(foodDao.existsById(cre.getId())) {
			return 0;
		}else {
			foodDao.save(cre);
			return 1;
		}
		
	}

}
