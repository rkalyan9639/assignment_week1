package com.grtlearning.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.grtlearning.bean.Credential;
import com.grtlearning.bean.Foods;
import com.grtlearning.bean.Orders;
import com.grtlearning.service.AdminService;
import com.grtlearning.service.CredentialService;
import com.grtlearning.service.FoodService;

@RestController
@RequestMapping("/admin")
@CrossOrigin
public class AdminController {
	@Autowired
	AdminService adminService;
	
	@GetMapping (value="loginn",produces = MediaType.APPLICATION_JSON_VALUE)
	public String adminloginPage(@RequestParam("username") String username ,@RequestParam("passward") String passward) {
		int res= adminService.adminLoginInfo(username ,passward);
		if(res==1) {
			return "YOU SIGNIN SUCCESSFULLY";
			
		}else {
			return "YOUR CREDENTIALS ARE WRONG PLEASE CHECK EMAIL AND PASSWARD";
		}
	}
	@Autowired
	FoodService foodService;
	@GetMapping(value="adminfoodlist")
	public List<Foods> listFoods() {
		return foodService.getListOfFood();
		
	}
	
	@Autowired
	CredentialService credentialService;
	@GetMapping(value="usercredential")
	public List<Credential> userlist() {
		return credentialService.getCredential();
	}
	
	//user delete by admin or crud operation
	@DeleteMapping (value="userdelete/{id}")
	public String userDeleteInfo(@PathVariable("id") String id ) {
		return  credentialService.userDel(id);
		
		
	}
	
	//food delete by admin
	@DeleteMapping (value="adminfooddelete")
	public String adminfoodDeleteInfo(@PathVariable("id") int id ) {
		int res= foodService.adminDelInfo(id);
		if(res==0) {
			return "not deleted";
			
		}else {
			return "deleted from dish list ";
			
		}
	}
	
//	@Autowired
//	OrderService orderService;
//	
//	@RequestMapping (value="totalbill_page")
//	public String userOrderPage(HttpSession hs) {
//		//System.out.println(price);
//	List<Orders> res= orderService.getAllBillOrder();
//	
//	hs.setAttribute("res1", res);
//	//hs.setAttribute("res", sum);
//	return "allorders";
//}
	
	
	@RequestMapping (value="getname_page")
	public String userNamePage() {
	List<Credential> ulistt=credentialService.getCredential();

	return "allname";
}
	
//	@RequestMapping (value="bill_byname", method = RequestMethod.POST)
//	public String userOrderPage(HttpSession hs ,@RequestParam String name) {
//		
//		//System.out.println(price);
//	List<Orders> res= orderService.getBillOrder(name);
//	if(res==null) {
//		
//	}else {
//		Iterator<Orders> li = res.iterator();
//		while(li.hasNext()){
//			Orders p = li.next();
//			
//		}	
//	}
//	//System.out.println(sum);
//	hs.setAttribute("res1", res);
//	//hs.setAttribute("res", sum);
//	return "allorders";
//}
//	
	@RequestMapping (value="food_admin_page", method = RequestMethod.POST)
	public String userSignPage(HttpSession hs ,@RequestParam("id") int id,@RequestParam("dish") String dish ,@RequestParam int price,@RequestParam String uname) {
		Foods cre=new Foods();
		cre.setId(id);
		cre.setDish(dish);
		cre.setPrice(price);
		cre.setUname(uname);
		System.out.println(dish);
		int res= foodService.storeFood(cre);
		if(res==1) {
			String msgg="food details store";
			hs.setAttribute("obj4", msgg);
			return "adminfood";
		}else {
			String msgg="food details store";
			hs.setAttribute("obj4", msgg);	
			return "adminfood";
		}
		
	}
	

}
