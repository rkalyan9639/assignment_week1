package com.grtlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.grtlearning")
@EnableJpaRepositories(basePackages = "com.grtlearning.dao")
@EntityScan(basePackages = "com.grtlearning.bean")
public class MIniProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MIniProjectApplication.class, args);
		System.err.println("running");
	}

}
