package com.grtlearning.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Orders {
	@Id
private int a;
private int id;
private String dish;
private int price;
private String name;

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getDish() {
	return dish;
}
public void setDish(String dish) {
	this.dish = dish;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

}
