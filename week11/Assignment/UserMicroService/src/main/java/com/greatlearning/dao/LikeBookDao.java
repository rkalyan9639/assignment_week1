package com.greatlearning.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.likeBooks;
@Repository
public interface LikeBookDao extends JpaRepository<likeBooks, Integer> {
	 @Query("select p from likeBooks p where p.uname=:uname")
	List<likeBooks> getByName(@Param("uname") String uname);

}
