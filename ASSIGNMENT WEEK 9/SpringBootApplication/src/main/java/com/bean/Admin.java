package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Entity
@RequiredArgsConstructor
@ToString
public class Admin {
	@Id
private String username;
private String passward;
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassward() {
	return passward;
}
public void setPassward(String passward) {
	this.passward = passward;
}

}
