package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Admin;
import com.bean.Book;
import com.bean.User;
import com.service.AdminService;
import com.service.BookService;

@RestController
@RequestMapping("admin")
public class AdminController {
	@Autowired
	AdminService adminService;
	@PostMapping(value = "registeruser",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String registerAdmin(@RequestBody Admin ad) {
		return adminService.registerAdmin(ad);
	}
	@GetMapping(value="logout")
	public String logoutAdmin() {
		return "logout Successfully";
	}

	@GetMapping(value = "login/{username}/{passward}")
	public String loginAdmin(@PathVariable("username") String username ,@PathVariable("passward") String passward) {
		return adminService.adminLoginDetails(username, passward);
		
	}
	
}