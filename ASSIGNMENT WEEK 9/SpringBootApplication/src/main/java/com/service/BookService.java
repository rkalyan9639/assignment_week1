package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Book;
import com.dao.BookDao;

@Service
public class BookService {
	@Autowired
	BookDao bookDao;
	public String StoreBookDetails(Book book) {
		if(bookDao.existsById(book.getBookid())) {
			return "Book with this id is already present please change book id";
		}else {
			bookDao.save(book);
			return "Book stored";
		}
	}
	public List<Book> getAllBook(){
		return bookDao.findAll();
	}
	
	public String deleteBookById(int bookid) {
		if(!bookDao.existsById(bookid)) {
			return "Book with this id is not present";
		}else {
			bookDao.deleteById(bookid);
			return "Book deleted successfully";
		}
	}
	public String updateBookDetails(Book book) {
		if(!bookDao.existsById(book.getBookid())) {
			return "book Details not present";
		}else{
			Book bb=bookDao.getById(book.getBookid());
			bb.setGeners(book.getGeners());
			bb.setPrice(book.getPrice());
			bookDao.saveAndFlush(bb);
		return "Book Details udated";
		}
	}
}
