package com.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bean.User;
import com.dao.UesrDao;
@Service
public class UserService {
	@Autowired
	UesrDao userDao;
	public List<User> getAllUserDetails() {
		return userDao.findAll();
	}
	
	public String storeUserInfo(User uu) {
		
		if(userDao.existsByEmail(uu.getEmail())) {
			return "This user is already present";
		}else {
			userDao.save(uu);
			return "Users Details Stored Successfully stored successfully";
		}
	}
	
	public String deleteUser(String email) {
		if(!userDao.existsByEmail(email)) {
			return "user details not present for this user";
			}else {
			userDao.deleteById(email);
			return "User details deleted successfully";
			}	
	}
	
	public String updateUserInfo(User uu) {
		if(!userDao.existsByEmail(uu.getEmail())) {
			return "user details not present for this user";
			}else {
			User u= userDao.getById(uu.getEmail());	// if product not present it will give exception 
			u.setName(uu.getName());						// existing product price change 
			userDao.saveAndFlush(u);				// save and flush method to update the existing product
			return "user updated successfully";
			}	
	}

}
