package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Admin;
import com.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	
	public String registerAdmin(Admin ad) {
			adminDao.save(ad);
			return "Admin Registered Successfully";
	}

	public String adminLoginDetails(String username, String passward) {
		Admin ad=adminDao.existsByEmail(username, passward);
		if(ad==null) {
			return "Account credentials are not match please try again";
		}else {
			return "Login Successfully";
		}
	}

}
