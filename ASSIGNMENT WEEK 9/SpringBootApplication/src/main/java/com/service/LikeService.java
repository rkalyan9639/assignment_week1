package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.bean.LikeBook;
import com.dao.LikeDao;

@Service
public class LikeService {
@Autowired
LikeDao likeDao;
public String StoreBookDetails(LikeBook book) {
	if(likeDao.existsById(book.getBookid())) {
		return "Book with this id is already present in like section please change book id";
	}else {
		likeDao.save(book);
		return "Book stored";
	}
}
public List<LikeBook> getAllBook(){
	return likeDao.findAll();
}

public String deleteBookById(int bookid) {
	if(!likeDao.existsById(bookid)) {
		return "Book with this id is not present";
	}else {
		likeDao.deleteById(bookid);
		return "Book deleted successfully";
	}
}
}
