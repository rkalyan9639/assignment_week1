
package com.grtlearning.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.grtlearning.bean.Foods;
import com.grtlearning.service.FoodService;
@Controller
public class FoodController {
	@Autowired
	FoodService foodService;
	@GetMapping(value="foodlist")
	
	public String listFood(HttpSession hs) {
		List<Foods> listt=foodService.getListOfFood();
		hs.setAttribute("listt", listt);
		return "foodlistt";
	}
	
}