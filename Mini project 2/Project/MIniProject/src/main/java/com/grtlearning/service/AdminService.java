package com.grtlearning.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grtlearning.bean.Admin;
import com.grtlearning.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	public int adminLoginInfo(String username, String passward) {
		Admin ad=adminDao.existsByEmail(username, passward);
		if(ad==null) {
			return 0;
		}else {
			return 1;
		}
	
	}

}
