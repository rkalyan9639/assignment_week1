package com.grtlearning.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.grtlearning.bean.Admin;

@Repository
public interface AdminDao extends JpaRepository<Admin,String> {

//	Admin existsByEmail(String username, String passward);
	@Query("select aa from Admin aa where aa.username=:username and aa.passward= :passward")
	Admin existsByEmail(@Param("username") String username,@Param("passward") String passward) ;
	//public int adminLoginInfo(String username, String passward);

}
