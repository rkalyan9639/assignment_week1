package com.grtlearning.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.grtlearning.bean.Credential;
import com.grtlearning.bean.Foods;
import com.grtlearning.bean.Orders;
import com.grtlearning.service.CredentialService;
import com.grtlearning.service.FoodService;
import com.grtlearning.service.OrderService;
import com.mysql.cj.x.protobuf.MysqlxCrud.Order;

@Controller
public class UserController {
	@Autowired
	CredentialService credentialService;
	@Autowired
	FoodService foodService;
	@RequestMapping (value="login",method = RequestMethod.POST)
	public String userloginPage(HttpSession hs ,@RequestParam("email") String email ,@RequestParam("passward") String passward) {
		
		System.out.println(email);
		Credential res= credentialService.userLoginInfo(email ,passward);
		if(res==null) {
			String msgg="YOU ARE NOT LOGIN PLEASE LOGIN AGAIN TO SEE THE FOODLIST";
			hs.setAttribute("obj1", msgg);
			return "userafterlogin";
		}else {	
			String re=res.getUname();
			hs.setAttribute("obj", re);
			List<Foods> listt=foodService.getListOfFood();
			hs.setAttribute("listt", listt);
			return "foodlistt";
		}
	
	}
	
	
	
	@RequestMapping (value="action_page", method = RequestMethod.POST)
	public String userSignPage(HttpSession hs ,@RequestParam("email") String email,@RequestParam("passward") String passward ,@RequestParam String name) {
		Credential cre=new Credential();
		cre.setEmail(email);
		cre.setPassward(passward);
		cre.setUname(name);
		System.out.println(email);
		int res= credentialService.storeCredentialsInfo(cre);
		if(res==1) {
			String msgg="YOU SIGNIN SUCCESSFULLY PLEASE LOGIN AGAIN TO SEE THE BOOKS";
			hs.setAttribute("obj1", msgg);
			return "userafterlogin";
		}else {
				
			return "foodlistt";
		}
		
	}
	
	@Autowired
	OrderService orderService;
	@RequestMapping (value="order_page", method = RequestMethod.POST)
	public String userOrderPage(HttpSession hs ,@RequestParam("id") int id,@RequestParam("dish") String dish ,@RequestParam int price ,@RequestParam String name) {
		Orders cre=new Orders();
		cre.setId(id);;
		cre.setDish(dish);
		cre.setPrice(price);
		cre.setName(name);
		
		System.out.println(price);
	int res= orderService.storeOrder(cre);
//		if(res==1) {
//			String msgg="YOU SIGNIN SUCCESSFULLY PLEASE LOGIN AGAIN TO SEE THE BOOKS";
//			return "index";
//		}else {
//				
			return "foodlistt";
	//}
		
	}
	
	
	@RequestMapping (value="bill_page", method = RequestMethod.POST)
	public String userOrderPage(HttpSession hs ,@RequestParam String name) {
		int sum=0;
		//System.out.println(price);
	List<Orders> res= orderService.getBillOrder(name);
	if(res==null) {
		
	}else {
		Iterator<Orders> li = res.iterator();
		while(li.hasNext()){
			Orders p = li.next();
			sum+=p.getPrice();
		}	
	}
	System.out.println(sum);
	hs.setAttribute("res1", res);
	hs.setAttribute("res", sum);
	return "bills";
}
}
