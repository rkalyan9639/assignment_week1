package com.grtlearning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.grtlearning")

public class MIniProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MIniProjectApplication.class, args);
		System.out.println("running");
	}

}
