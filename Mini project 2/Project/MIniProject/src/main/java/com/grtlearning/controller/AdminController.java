package com.grtlearning.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.grtlearning.bean.Credential;
import com.grtlearning.bean.Foods;
import com.grtlearning.bean.Orders;
import com.grtlearning.service.AdminService;
import com.grtlearning.service.CredentialService;
import com.grtlearning.service.FoodService;
import com.grtlearning.service.OrderService;

@Controller
public class AdminController {
	@Autowired
	AdminService adminService;
	
	@RequestMapping (value="loginn",method = RequestMethod.POST)
	public String adminloginPage(HttpSession hs,@RequestParam("username") String username ,@RequestParam("passward") String passward) {
		System.out.println(username);
		int res= adminService.adminLoginInfo(username ,passward);
		if(res==1) {
			String msgg="YOU SIGNIN SUCCESSFULLY";
			hs.setAttribute("obj3", msgg);
			return "adminafterlogin";
		}else {
				
			return "foodlistt";
		}
	}
	@Autowired
	FoodService foodService;
	@GetMapping(value="adminfoodlist")
	public String listFoods(HttpSession hs) {
		List<Foods> listt=foodService.getListOfFood();
		hs.setAttribute("listt", listt);
		return "adminfood";
	}
	
	@Autowired
	CredentialService credentialService;
	@GetMapping(value="admincredential")
	public String userlist(HttpSession hs) {
		List<Credential> ulistt=credentialService.getCredential();
		hs.setAttribute("userlistt", ulistt);
		return "useroperation";
	}
	
	//user delete by admin or crud operation
	@RequestMapping (value="admindelete")
	public String adminDeleteInfo(@RequestParam("email") String email ) {
		int res= credentialService.adminDelInfo(email);
		if(res==1) {
			String msgg="YOU SIGNIN SUCCESSFULLY PLEASE LOGIN AGAIN TO SEE THE BOOKS";
			return "adminafterlogin";
		}else {
				
			return "foodlistt";
		}
	}
	
	//food delete by admin
	@RequestMapping (value="adminfooddelete")
	public String adminfoodDeleteInfo(HttpSession hs,@RequestParam("id") int id ) {
		int res= foodService.adminDelInfo(id);
		if(res==0) {
			String msgg="not deleted";
			hs.setAttribute("obj4", msgg);
			return "adminafood";
		}else {
			String msgg="deleted from dish list ";
			hs.setAttribute("obj4", msgg);	
			return "adminfood";
		}
	}
	
	@Autowired
	OrderService orderService;
	
	@RequestMapping (value="totalbill_page")
	public String userOrderPage(HttpSession hs) {
		//System.out.println(price);
	List<Orders> res= orderService.getAllBillOrder();
	
	hs.setAttribute("res1", res);
	//hs.setAttribute("res", sum);
	return "allorders";
}
	
	
	@RequestMapping (value="getname_page")
	public String userNamePage(HttpSession hs) {
		//System.out.println(price);
	//List<Orders> res= orderService.getAllBillOrder();

	List<Credential> ulistt=credentialService.getCredential();
	hs.setAttribute("res1", ulistt);
	return "allname";
}
	
	@RequestMapping (value="bill_byname", method = RequestMethod.POST)
	public String userOrderPage(HttpSession hs ,@RequestParam String name) {
		
		//System.out.println(price);
	List<Orders> res= orderService.getBillOrder(name);
	if(res==null) {
		
	}else {
		Iterator<Orders> li = res.iterator();
		while(li.hasNext()){
			Orders p = li.next();
			
		}	
	}
	//System.out.println(sum);
	hs.setAttribute("res1", res);
	//hs.setAttribute("res", sum);
	return "allorders";
}
	
	@RequestMapping (value="food_admin_page", method = RequestMethod.POST)
	public String userSignPage(HttpSession hs ,@RequestParam("id") int id,@RequestParam("dish") String dish ,@RequestParam int price,@RequestParam String uname) {
		Foods cre=new Foods();
		cre.setId(id);
		cre.setDish(dish);
		cre.setPrice(price);
		cre.setUname(uname);
		System.out.println(dish);
		int res= foodService.storeFood(cre);
		if(res==1) {
			String msgg="food details store";
			hs.setAttribute("obj4", msgg);
			return "adminfood";
		}else {
			String msgg="food details store";
			hs.setAttribute("obj4", msgg);	
			return "adminfood";
		}
		
	}
	
}
