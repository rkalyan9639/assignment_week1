package com.grtlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grtlearning.bean.Orders;
import com.grtlearning.dao.OrderDao;

@Service
public class OrderService {
	@Autowired
	OrderDao orderDao;
	public int storeOrder(Orders order) {
		orderDao.save(order);
		return 1;
	}
	public List<Orders> getBillOrder(String name) {
		List<Orders> list=orderDao.getByname(name);
		if(list==null) {
		return null;
	 }else {
		return orderDao.getByname(name);
	}
}
	public List<Orders> getAllBillOrder() {
		List<Orders> list=orderDao.findAll();
		if(list==null) {
		return null;
	 }else {
		return orderDao.findAll();
	}
}
}