<%@page import="java.util.Iterator"%>
<%@page import="com.grtlearning.bean.Foods"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
table, th, td {
  border:1px solid black;
  width:100px;
  textalign:center;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background-color: #328f8a;
  background-image: linear-gradient(45deg,#328f8a,#08ac4b);
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
</style>
</head>
<body>
<div>
<%
	Object obj1=session.getAttribute("obj4");
	
%>
<h4><%=obj1 %> Refresh to see</h4>
<button><a href="adminfoodlist">refresh</a></button>
        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
                <tr style="width:100px">
                    <th>id</th>
                    <th>dish</th>
                    <th>price</th>
                    <th>delete item</th>
                    
                </tr>
            </thead>
            <tbody>
            	<%
		Object obj = session.getAttribute("listt");
		List<Foods> listOffood = (List<Foods>)obj;
		Iterator<Foods> li = listOffood.iterator();
		while(li.hasNext()){
			Foods p = li.next();
			%>
				<tr>
				<td><%=p.getId() %></td>
				<td><%=p.getDish() %></td>
				<td><%=p.getPrice() %></td>
				<td>
				<form action="adminfooddelete">
					<input type="hidden"name="id" value=<%=p.getId() %>>
					<input type="submit" value="delete">
				</form>
				</td>
				
			<% 
		}
		%>
		<hr>
		<h3>IF YOU WANT TO ADD NEW ITEM ADD FROM HERE</h3>
		  <form class="login-form" action="food_admin_page" method="post" >
		  <label>ID		</label>
          <input type="text"  name="id" placeholder="id" required><br><br>
          <label>DISH	</label>
          <input type="text"  name="dish" placeholder="dish" required><br><br>
          <label>PRICE	</label>
           <input type="text" name="price" placeholder="price" required><br><br>
           <label>ANY NAME</label>
           <input type="name" name="uname" placeholder="anyname" required><br><br>
          <button type="submit">ADD ITEM</button><br><br>
        </form>
		         <hr>           
               </tr>     
                
            </tbody>
        </table>
    </div>
</div>
</body>
</html>