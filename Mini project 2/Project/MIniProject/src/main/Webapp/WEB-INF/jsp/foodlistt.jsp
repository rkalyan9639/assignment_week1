<%@page import="java.util.Iterator"%>
<%@page import="com.grtlearning.bean.Foods"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="ISO-8859-1">
    <title>List Users</title>
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/css/bootstrap.min.css" />
    <script type="text/javascript" src="/webjars/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/webjars/bootstrap/js/bootstrap.min.js"></script>
</head>
<style>
table, th, td {
  border:1px solid black;
}
</style>

<body style="text-align:center">
<%
	Object obj1=session.getAttribute("obj");
	
%>
<h4>YOU ARE LOGIN SUCCESS FULLY  <%=obj1 %></h4>
<div><h2>LIST OF FOOD</h2></div>
 <div>

        <table class="table table-striped table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th>id</th>
                    <th>dish</th>
                    <th>price</th>
                    <th>order</th>
                </tr>
            </thead>
            <tbody>
            	<%
        
            	
		Object obj = session.getAttribute("listt");
		List<Foods> listOffood = (List<Foods>)obj;
		Iterator<Foods> li = listOffood.iterator();
		while(li.hasNext()){
			Foods p = li.next();
			%>
				<tr>
				<td><%=p.getId() %></td>
				<td><%=p.getDish() %></td>
				<td><%=p.getPrice() %></td>
				<td><form action="order_page", method="post">
					<input type="hidden"name="id" value=<%=p.getId() %>>
					<input type="hidden" name="dish" value=<%=p.getDish() %>>
					<input type="hidden" name="price"value=<%=p.getPrice() %>>
					<input type="hidden" name="name" value=<%=obj1 %>>
					<input type="submit" value="order">
				</form></td>
			<% 
		}
		
		
		%>
		      <form action="bill_page", method="post">
					<input type="hidden" name="name" value=<%=obj1 %>>
					<input type="submit" value="show bills">
				</form>              
               </tr>     
                
            </tbody>
        </table>
    </div>
</div>
</body>
</html>