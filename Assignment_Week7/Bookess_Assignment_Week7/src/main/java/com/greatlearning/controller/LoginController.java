package com.greatlearning.controller;

import java.io.IOException;
import java.sql.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



import com.greatlearning.bean.Login;
import com.greatlearning.dao.LoginDao;
import com.greatlearning.resource.DbResource;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	public static boolean validate(Login login) throws ClassNotFoundException {
        boolean status = false;

        

        try { 
        	Connection con=DbResource.getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = con.prepareStatement("select * from login where user = ? and password = ? "); 
            preparedStatement.setString(1, login.getEmail());
            preparedStatement.setString(2, login.getPassword());

            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            status = rs.next();
           

        } catch (SQLException e) {
            // process sql exception
            System.out.println(e);
        }
        return status;
    }
        

    private void printSQLException(SQLException ex) {
        for (Throwable e: ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }
	public static int signUp(Login login) throws ClassNotFoundException {
        int result=0;
      
        try {
        	
        	Connection con=DbResource.getConnection();
            // Step 2:Create a statement using connection object
            PreparedStatement preparedStatement = con.prepareStatement("insert into login values(?,?)");
           
            preparedStatement.setString(1, login.getEmail());
            preparedStatement.setString(2, login.getPassword());
           

            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
             result = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            // process sql exception
            System.out.println(e);
        }
        return result;
    }

	public static void addLiked(String name, String password, String bookId) throws SQLException {
		Connection con=DbResource.getConnection();
		PreparedStatement st = con.prepareStatement("insert into liked(name,password,booksId) values(?,?,?)");
		PreparedStatement st2 = con.prepareStatement("select *from liked");
		ResultSet rs = st2.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(name) && rs.getString(2).equals(password) && rs.getString(3).equals(bookId)) {
				return;	
			}
		}
		st.setString(1, name);
		st.setString(2, password);
		st.setString(3, bookId);
		st.executeUpdate();	
		st.close();
		st2.close();
		rs.close();
		
	}
	public void addLater(String name, String password, String bookId) throws SQLException {
		Connection con=DbResource.getConnection();
		PreparedStatement	st = con.prepareStatement("insert into ReadLater(name,password,booksId) values(?,?,?)");
		PreparedStatement st2 = con.prepareStatement("select *from ReadLater");
		ResultSet rs = st2.executeQuery();
		while(rs.next()) {
		
			if(rs.getString(1).equals(name) && rs.getString(2).equals(password) && rs.getString(3).equals(bookId)) {
				return;	
			}
		}
		st.setString(1, name);
		st.setString(2, password);
		st.setString(3, bookId);
		st.executeUpdate();
		st.close();
		st2.close();
		rs.close();
		
	}
	
}

