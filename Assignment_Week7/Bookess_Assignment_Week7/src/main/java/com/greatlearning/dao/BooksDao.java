package com.greatlearning.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.greatlearning.bean.Books;
import com.greatlearning.resource.DbResource;

public class BooksDao {
      public List<Books> getAllBooks(){
            		List<Books> listOfBooks = new ArrayList<Books>();
            		try {
            			Connection con = DbResource.getConnection();
            			PreparedStatement psmt= con.prepareStatement("select * from books");
            			ResultSet rs = psmt.executeQuery();

            while (rs.next()) {
            	Books bb = new Books();
                bb.setBookId(rs.getInt("bookid"));
                bb.setBookTitle(rs.getString("booktitle"));
                bb.setbookAuthor(rs.getString("bookauthor"));
                bb.setbookPrice(rs.getInt("Price"));
                listOfBooks.add(bb);
            }

        } catch (SQLException e) {
            
            System.out.println(e);
        }
        return listOfBooks;
    }

	

}
