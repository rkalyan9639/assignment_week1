package com.greatlearning.bean;
public class SignUp {
private String first_name;
private String last_name;
private String emailId;
private String password;

public SignUp() {
	super();
	
}


public SignUp(String first_name, String last_name, String emailId, String password) {
	super();
	this.first_name = first_name;
	this.last_name = last_name;
	this.emailId = emailId;
	this.password = password;
	
}


public String getFirst_name() {
	return first_name;
}
public void setFirst_name(String first_name) {
	this.first_name = first_name;
}
public String getLast_name() {
	return last_name;
}
public void setLast_name(String last_name) {
	this.last_name = last_name;
}
public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}

@Override
public String toString() {
	return "SignUp [first_name=" + first_name + ", last_name=" + last_name + ", emailId=" + emailId + ", password="
			+ password + ",]";
}


}
