package com.greatlearning.bean;

public class Books {
	private int bookId;
	private String bookTitle;
	private String bookAuthor;
	private int bookPrice;
	
	public Books() {
		
	}
	
	public Books(int bookId, String bookTitle, String bookgenre, String image) {
		this.bookId = bookId;
		this.bookTitle = bookTitle;
		this.bookAuthor = bookAuthor;
		this.bookPrice = bookPrice;
	}

	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getbookAuthor() {
		return bookAuthor;
	}
	public void setbookAuthor(String bookAuthor) {
		this.bookAuthor = bookAuthor;
	}
	public int getbookPrice() {
		return bookPrice;
	}
	public void setbookPrice(int bookPrice) {
		this.bookPrice = bookPrice;
	}
	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookTitle=" + bookTitle + ", bookAuthor=" + bookAuthor + ", bookPrice=" + bookPrice
				+ "]";
	}
	
}