create database Movies_Kalyan;
use Movies_Kalyan;



create table Movies_Coming(ID int,Title varchar(30),Year int,Genre varchar(50),Ratings int,Category varchar(30));


create table Movies_inTheatres(ID int,Title varchar(30),Year int,Genre varchar(50),Ratings int,Category varchar(30));

create table Top_RatedIndia(ID int,Title varchar(30),Year int,Genre varchar(50),Ratings int,Category varchar(30));

create table Top_RatedMovies (ID int,Title varchar(30),Year int,Genre varchar(50),Ratings int,Category varchar(30));


insert into Movies_Coming values(1,'The Wolf Of Wall Street',2013,'Real Story',9,'Movies Coming');
insert into Movies_Coming values(2,'The Big Short',2008,'Housing loans ',10,'Movies Coming');
insert into Movies_coming values(3,'Pursuit Of Happiness',2006,'Self Belief',6,'Movies Coming');
insert into Movies_coming values(4,'Money Heist',2020,'The Professor',7,'Movies Coming');
insert into Movies_coming values(5,'The Jobs',2014,'Steve Jobs Autobiography',8,'Movies Coming');




insert into Movies_inTheatres values(1,'Mahanati',2017,'Sridevi Autobiography',9,'Movies in Theatres');
insert into Movies_inTheatres values(2,'Panja',2013,'Nature and society',10,'Movies in Theatres');
insert into Movies_inTheatres values(3,'Julayi',2019,'Bank robbery',6,'Movies in Theatres');
insert into Movies_inTheatres values(4,'Jalsa',2011,'Children and love',7,'Movies in Theatres');
insert into Movies_inTheatres values(5,'Fight Club',2016,'College life of students',8,'Movies in Theatres');




insert into Top_RatedIndia values(1,'Four More Shots',2017,'Life in mumbai city',7,'Top Rated India');
insert into Top_RatedIndia values(2,'The titanic',2011,'Titanic ship destroyed',8,'Top Rated India');
insert into Top_RatedIndia values(3,'Varsham',2013,'Invloves love',10,'Top Rated India');
insert into Top_RatedIndia values(4,'Aashiqui2',2012,'Singer life',9,'Top Rated India');
insert into Top_RatedIndia values(5,'7th Sense',2011,'Ara of diseases',6,'Top Rated India');


insert into Top_RatedMovies values(1,'Hello',2015,'Family movie',9,'Top Rated Movies');
insert into Top_RatedMovies values(2,'Josh',2012,'Hyderabad student life',10,'Top Rated Movies');
insert into Top_RatedMovies values(3,'Arjun Reddy',2019,'love breakup story',7,'Top Rated Movies');
insert into Top_RatedMovies values(4,'S/O Satyamurthy',2015,'Life of ups and downs',8,'Top Rated Movies');
insert into Top_RatedMovies values(5,'King',2008,'Leader of the colony',6,'Top Rated Movies');


select*from Movies_Coming;
select*from Top_RatedIndia;
select*from Top_RatedMovies;
select*from Movies_inTheatres;
