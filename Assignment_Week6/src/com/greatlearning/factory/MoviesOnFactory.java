package com.greatlearning.factory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.MoviesComing;
import com.greatlearning.bean.MoviesInTheatre;
import com.greatlearning.bean.TopRatedIndia;
import com.greatlearning.bean.TopRatedMovies;
import com.greatlearning.db.DbResource;

interface MovieOnline  {
	
	public List<String> movieType() throws Exception ;

}
//From here the factory pattern starts
class MovieComing implements MovieOnline{
	
	
	Connection con = DbResource.getDBConnection();
	public MovieComing() {}
	@Override
	public List<String> movieType() throws Exception {
		
		try {
			List<MoviesComing> movies=new ArrayList<MoviesComing>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movie_Comming");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				MoviesComing movie=new MoviesComing();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenre(rs.getString(4));
				movie.setRatings(rs.getInt(5));
				movie.setCategory(rs.getString(6));
				movies.add(movie);
			}}catch(Exception e) {
				System.out.println("Movie_Coming"+e);
			}
			
		
		return null;
	}
	
}
class MovieInTheatres implements MovieOnline{
	//Here we are connecting the database
	Connection con = DbResource.getDBConnection();
	public MovieInTheatres() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<MoviesInTheatre> movies=new ArrayList<MoviesInTheatre>();
			PreparedStatement pstmt=con.prepareStatement("select * from Movies_In_Theaters");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				MoviesInTheatre movie=new MoviesInTheatre();
				movie.setId(rs.getInt(1));
				movie.setTitle(rs.getString(2));
				movie.setYear(rs.getInt(3));
				movie.setGenre(rs.getString(4));
				movie.setRatings(rs.getInt(5));
				movie.setCategory(rs.getString(6));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Movies_In_Theaters"+e);
		}
	    return null;	
	}
	
}
class TopRatedIndianMovie implements MovieOnline{
	Connection con = DbResource.getDBConnection();
	public TopRatedIndianMovie() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<TopRatedIndia> movies=new ArrayList<TopRatedIndia>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Indianmovie");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				TopRatedIndia movie=new TopRatedIndia();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenre(rs.getString(3));
				movies.add(movie);
				
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Indianmovi"+e);
		}
	    return null;	
	}
	
	
}
class TopRatedMovie implements MovieOnline{
	Connection con = DbResource.getDBConnection();
	public TopRatedMovie() {}
	@Override
	public List<String> movieType() throws Exception {
		try {
			List<TopRatedMovies> movies=new ArrayList<TopRatedMovies>();
			PreparedStatement pstmt=con.prepareStatement("select * from Top_Rated_Movie");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next()) {
				TopRatedMovies movie = new TopRatedMovies();
				movie.setTitle(rs.getString(1));
				movie.setYear(rs.getInt(2));
				movie.setGenre(rs.getString(3));
				movie.setRatings(rs.getInt(4));
				movie.setCategory(rs.getString(5));
				movies.add(movie);
			}
		}catch(Exception e) {
			System.out.println("Top_Rated_Movie"+e);
		}
	    return null;	
	}
}
	
class MoviesFactory{
	public static MovieOnline getInstance(String type) {
		if(type.equalsIgnoreCase("MovieComing")) {
			return new MovieComing();
		}else if(type.equalsIgnoreCase("MovieInTheatre")) {
			return new MovieInTheatres();
		}else if(type.equalsIgnoreCase("TopRatedIndia")) {
			return new TopRatedIndianMovie();
		}
		else if(type.equalsIgnoreCase("TopRatedMovies")) {
			return new TopRatedMovie();
		}else {
	    	return null;
	}
}
}

public class MoviesOnFactory {
	public static void main(String[] args) throws Exception {
		
		MovieOnline mo =MoviesFactory.getInstance("MovieComing");
		mo.movieType();
	    MovieOnline mo1 =MoviesFactory.getInstance("MoviesInTheatre");
	    mo1.movieType();
	    MovieOnline mo2 =MoviesFactory.getInstance("TopRatedIndia");
	    mo2.movieType();
	    MovieOnline mo3=MoviesFactory.getInstance("TopRatedMovies");
	    mo3.movieType();
	    
	}
}
			
		

	