package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

 

import com.greatlearning.bean.MoviesInTheatre;
import com.greatlearning.db.DbResource;


public class MoviesInTheatreDao {
	public List<MoviesInTheatre> findAllMovie() {
		List<MoviesInTheatre> listOfMovie = new ArrayList<>();
		try {
			Connection con = DbResource.getDBConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from product");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				MoviesInTheatre Mit = new MoviesInTheatre();
					Mit.setId(rs.getInt(1));
					Mit.setTitle(rs.getString(2));
					Mit.setYear(rs.getInt(3));
					Mit.setGenre(rs.getString(4));
					Mit.setRatings(rs.getInt(5));
					Mit.setCategory(rs.getString(6));
					
					listOfMovie.add(Mit);
			}
			} catch (Exception e) {
				System.out.println("The list of movies which are not found on database"+e);
			}
		return listOfMovie;


	}
}
