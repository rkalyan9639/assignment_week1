package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;



import com.greatlearning.bean.TopRatedIndia;
import com.greatlearning.db.DbResource;


public class TopRatedIndiaDao {
	public List<TopRatedIndia> findAllMovies() {
		List<TopRatedIndia> listOfMovie = new ArrayList<>();
		try {
			Connection con = DbResource.getDBConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from product");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				TopRatedIndia Tri = new TopRatedIndia();
					Tri.setTitle(rs.getString(1));
					Tri.setYear(rs.getInt(2));
					Tri.setGenre(rs.getString(3));
					Tri.setRatings(rs.getInt(4));
					Tri.setCategory(rs.getString(5));
					
					listOfMovie.add(Tri);
			}
			} catch (Exception e) {
				System.out.println("The list of movies which are not found on database"+e);
			}
		return listOfMovie;


	}
}