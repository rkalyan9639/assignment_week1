package com.greatlearning.db;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbResource {
	private static Connection connec;
	
	
	private DbResource() {	
		super();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connec = DriverManager.getConnection("jdbc:mysql://localhost:3306/Assignment_Week6", "root", "Kalyan@45");
		} 
		catch (Exception e) {
			System.out.println("Database-Connection Error " + e);
		}

	}
		
	private static final DbResource instance = new DbResource();
	public static DbResource getInstance() {
		return instance;
	}
	
	public static Connection getDBConnection() {
		try {
			return connec;
		}
		catch(Exception e) {
		}
		return null;
	}
}
