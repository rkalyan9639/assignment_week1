package com.greatlearning.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.TopRatedIndia;
import com.greatlearning.dao.TopRatedIndiaDao;

public class TopRatedIndiaTest {
	TopRatedIndiaDao es = new TopRatedIndiaDao();
	List<TopRatedIndia> listOfmovies = es.findAllMovies();
	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		TopRatedIndia movie = listOfmovies.get(2);
		assertEquals(3, movie.getId());

	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		TopRatedIndia movie = listOfmovies.get(2);
		assertEquals("Four More Shots", movie.getTitle());

	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented")TopRatedIndian movie = listOfmovies.get(2);
		TopRatedIndia movie = listOfmovies.get(2);
		assertEquals(2017, movie.getYear());
	}

	
	public void testGetGener() {
		//fail("Not yet implemented");
		TopRatedIndia movie = listOfmovies.get(2);
		assertEquals("Singer Life", movie.getGenre());

	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		TopRatedIndia movie = listOfmovies.get(2);
		assertEquals("8", movie.getRatings());
	}
	
	
	
	@Test
	public void testCategory() {
		//fail("Not yet implemented");
		TopRatedIndia movie = listOfmovies.get(2);
		assertEquals("TopRatedIndia", movie.getRatings());
	}

}
