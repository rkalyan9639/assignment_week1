package com.hcl.angular.model;

public enum ERole {
	ROLE_USER,
	ROLE_MODERATOR,
	ROLE_ADMIN

}
