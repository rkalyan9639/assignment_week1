package com.greatlearning.controller;

import java.sql.PreparedStatement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.greatlearning.bean.User;
import com.greatlearning.dao.UserDao;

@Controller
public class SignUpController {
	@Autowired
	UserDao userDao;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("user") User user) {
		userDao.save(user);
		return "index";
	}
}

