package com.greatlearning.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.greatlearning.bean.Books;

@Repository
public class BooksDao {
		@Autowired
		static
		JdbcTemplate jdbcTemplate;
       public static  List<Books> getBooks() {
			return jdbcTemplate.query("select * from books", new BooksRowMapper());
		}
	}
	class BooksRowMapper implements RowMapper<Books>{
		@Override
				public Books mapRow(ResultSet rs, int row) throws SQLException {
					Books book = new Books();
					book.setBook_id(rs.getInt(1));
					book.setTitle(rs.getString(2));
					book.setAuthor(rs.getString(3));
					book.setPrice(rs.getInt(4));
					return book;
			}
	}
