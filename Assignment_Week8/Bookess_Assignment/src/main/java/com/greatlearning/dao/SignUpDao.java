package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.greatlearning.bean.SignUp;

public class SignUpDao {

	@Autowired
	private JdbcTemplate template;


	public boolean SignUpUser(String username, String password) throws DuplicateKeyException{
		
		String sql = "insert into BookUsers values (?,?)";
		 if(template.update(sql, username, password)!=0)
			return true;
		else
			throw new DuplicateKeyException("Username Already Exists! Try with another name.");		
	}
}
